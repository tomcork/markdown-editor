import React from 'react';
import './App.css';
import EditorPreviewerPane from './components/EditorPreviewerPane';

class App extends React.Component {
  componentDidMount() {
    // Uncomment to add the test suite to the page.
    
    // const script = document.createElement("script");
    // script.src = "https://cdn.freecodecamp.org/testable-projects-fcc/v1/bundle.js";
    // script.async = true;

    // document.body.appendChild(script);
  }

  render() {
    return (
      <div className="App">
        <EditorPreviewerPane />
      </div>
    );
  }
}

export default App;
