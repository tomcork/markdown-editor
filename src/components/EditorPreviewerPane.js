import React from 'react';
import Editor from './Editor';
import Previewer from './Previewer';
import marked from 'marked';

class EditorPreviewerPane extends React.Component {
    constructor(props) {
        super(props);

        this.defaultEditorText = "# Tom Cork's Markdown Editor\n" +
            "## Introduction\n" +
            "This is a quick sample of the markdown editor I have created in the browser.\n" +
            "At the moment it is very barebones, but **in the future** it will become better.\n" + 
            "## Test area\n" +
            "[I'm an inline-style link](https://www.google.com) \n"+
            "\nThis is possible using the `marked.js` library. \n" + 
            "``` \nBlock code\nThis is another line\n```\n" + 
            "- Sometimes lists are required \n\n" +
            "> Blockquotes are very handy in email to emulate reply text.\n" + 
            "> This line is part of the same quote.\n\n" +
            "Built using react: ![alt text](https://cdn4.iconfinder.com/data/icons/logos-3/600/React.js_logo-512.png) \n\n";



        marked.setOptions({
            gfm: true,
            breaks: true
        });

        this.state = {
            previewContent: marked(this.defaultEditorText)
        };
        
        this.onEditorContentChange = this.onEditorContentChange.bind(this);
    }

    onEditorContentChange(event) {
        var markup = marked(event.target.value, {sanitize: true});

        this.setState({
            previewContent: markup
        });
    }

    render() {
        return (
            <div className="EditorPreviewerPane">
                <Editor defaultText={this.defaultEditorText} id="editor" onEditorContentChange={this.onEditorContentChange}/>
                <Previewer >
                    {this.state.previewContent}
                </Previewer>
            </div>
        )
    }
}

export default EditorPreviewerPane;