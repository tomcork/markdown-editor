import React from 'react';
import PropTypes from 'prop-types'

class Editor extends React.Component {
    constructor (props) {
        super(props);

        this.state = {
            input: props.defaultText ? props.defaultText : ''
        }

        this.onInputChange = this.onInputChange.bind(this);
    }

    onInputChange(event) {
        this.setState({input: event.target.value});
        this.props.onEditorContentChange(event);
    }

    render() {
        return (
            <div>
                <textarea value={this.state.input} onChange={this.onInputChange} id={this.props.id}/>
            </div>
        );
    }
}

Editor.propTypes = {
    defaultText: PropTypes.string
};

export default Editor;