import React from 'react';

class Previewer extends React.Component {
    render() {
        return (
            <div id="preview" dangerouslySetInnerHTML={ {__html : this.props.children} } >

            </div>
        );
    }
}

export default Previewer;